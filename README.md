# golang-rest-redis



## Getting started
```
cd your_folder
git clone https://gitlab.com/personal-mj-project/golang-rest-redis.git
```

## Build Golang Rest Image
```
docker build --tag pnbas .
```

## First Deploy Redis
```
kubectl apply -f deployment.yml
```

## Second Deploy Glang Rest
```
kubectl apply -f deployment2.yml
```

## Insert Key
```
curl http://localhost:30080/pnbas/PNNO004-1000000005 --include --request "POST";
```

## Get Key
```
curl http://localhost:30080/pnbas/PNNO002-1000000002
```

## Get All Key
```
curl http://localhost:30080/pnbas/
```

## To Stop / Kill
```
kubectl delete -f deployment.yml
kubectl delete -f deployment2.yml
```
