package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
)

var (
	ctx    context.Context
	client *redis.Client
)

func main() {
	redisHost := "localhost"
	redisPort := "6379"

	// Check if an environment variable is set
	_, exists := os.LookupEnv("REDIS_PNBAS_SERVICE_HOST")
	if exists {
		redisHost = os.Getenv("REDIS_PNBAS_SERVICE_HOST")
		fmt.Println("Environment variable REDIS_PNBAS_SERVICE_HOST exists ", redisHost)
	}
	_, exists = os.LookupEnv("REDIS_PNBAS_SERVICE_PORT")
	if exists {
		redisPort = os.Getenv("REDIS_PNBAS_SERVICE_PORT")
		fmt.Println("Environment variable REDIS_PNBAS_SERVICE_PORT exists ", redisPort)
	}

	// Create a new Redis client
	client = redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort, // Redis server address
		Password: "",                          // Redis server password
		DB:       0,                           // Redis database number
	})

	ctx = context.Background()

	pong, err := client.Ping(ctx).Result()
	if err != nil {
		fmt.Println("Ping Error:", err)
	} else {
		fmt.Println(pong)
	}

	// initDataPartner()

	router := gin.Default()
	//Partner Map Bank Accounts
	router.GET("/pnbas", getPnbas)
	router.GET("/pnbas/:pnba", getPnba)
	router.POST("/pnbas/:pnba", postPnba)

	router.Run("0.0.0.0:8080")
}

func initDataPartner() {
	// read from persistent

	/*
		//100,000 keys
		for i := 1; i <= 1000000; i++ {
			keyVal := "bnkAcct000" + strconv.Itoa(i) + "-" + "bnkAcct000" + strconv.Itoa(i)
			//fmt.Println(keyVal)
			err := client.Set(ctx, keyVal, keyVal, 0).Err()
			if err != nil {
				fmt.Println("initDataPartner Error:", err)
				return
			}
		}
	*/
	fmt.Println("Finish initDataPartner")
}

func getPnbas(c *gin.Context) {
	// Use the `Keys` command to retrieve all keys matching a pattern
	keys, err := client.Keys(ctx, "*").Result()
	if err != nil {
		log.Fatal(err)
	}

	if len(keys) != 0 {
		c.IndentedJSON(http.StatusOK, keys)
	} else {
		responseNoContent(c)
	}
}

func postPnba(c *gin.Context) {
	pnba := c.Param("pnba")
	if len(pnba) == 0 {
		c.IndentedJSON(http.StatusUnprocessableEntity, pnba)
	}

	//Add to Redis
	err := client.Set(ctx, pnba, pnba, 0).Err()
	if err != nil {
		fmt.Println("Post Error:", err)
		responseInternalError(c)
		return
	}
	c.IndentedJSON(http.StatusCreated, pnba)
}

func getPnba(c *gin.Context) {
	pnba := c.Param("pnba")
	//Get from Redis
	val, err := client.Get(ctx, pnba).Result()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			responseNoContent(c)
		} else {
			fmt.Println("Get Error:", err)
			responseInternalError(c)
		}
	} else {
		if val != "" {
			c.IndentedJSON(http.StatusOK, val)
			return
		} else {
			responseNoContent(c)
		}
	}
}

func responseNoContent(c *gin.Context) {
	c.IndentedJSON(http.StatusNoContent, gin.H{"message": "Mapping Not Found"})
}

func responseInternalError(c *gin.Context) {
	c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": "Internal Error"})
}
