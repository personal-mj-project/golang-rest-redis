docker pull redis
docker run --name redis-ctn -p 6379:6379 -d redis
docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis 
docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data redis

docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis redis-server --save 60 1 --loglevel warning
docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis redis-server --appendonly yes --appendfilename "appendonly.aof"
docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis redis-server --appendonly yes


docker build --tag docker-rest-redis-partnerbankacct .

docker run --name partnerbankacct \
--env REDIS_PNTBNK_HOST=10.101.52.179 \
--env REDIS_PNTBNK_PORT=6379 \
-p 8080:8080 docker-rest-redis-partnerbankacct



docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis redis-server --appendonly yes
docker run --name partnerbankacct -p 8080:8080 -d docker-rest-redis-partnerbankacct
go run index.go


docker inspect redis-ctn
docker inspect redis-ctn | grep IP
docker inspect redis-ctn | grep IPAddress
docker inspect partnerbankacct
docker inspect partnerbankacct | grep IPAddress

docker network ls
docker network inspect bridge

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00001-100000111","ptnNo": "ptnNo00001","bnkAcct": "100000999"}';

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00002-100000111","ptnNo": "ptnNo00002","bnkAcct": "100000999"}';

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00001:100000994","ptnNo": "ptnNo00001","bnkAcct": "100000994"}';

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00001:100000995","ptnNo": "ptnNo00001","bnkAcct": "100000995"}';

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00007:100000997","ptnNo": "ptnNo00007","bnkAcct": "100000997"}';

curl http://localhost:8080/partnerMapBankAccounts  \
--include --header     "Content-Type: application/json" \
--request "POST" \
--data '{"ptnNoMapBnkAcct": "ptnNo00008:100000998","ptnNo": "ptnNo00008","bnkAcct": "100000998"}';

curl http://localhost:8080/partnerMapBankAccounts

curl http://localhost:8080/partnerMapBankAccounts/ptnNo00001-100000111
curl http://localhost:8080/partnerMapBankAccounts/ptnNo00002-100000456


curl http://localhost:8080/partnerMapBankAccounts/ptnNo00001-100000111







--========== NEW ==========--


kubectl apply -f deployment.yml
kubectl delete -f deployment.yml

kubectl apply -f deployment2.yml
kubectl delete -f deployment2.yml

kubectl get deployment
kubectl get pod
kubectl get service
kubectl get pv

kubectl port-forward deployment/partnerbankacct-deployment 8080:8080
kubectl port-forward service/partnerbankacct-service 8080:8080


curl http://localhost:8080/pnbas
curl http://localhost:30080/pnbas



---
IN REDIS TERMINAL
redis-cli
dbsize

127.0.0.1:6379> dbsize
(integer) 1000000

MEMORY USAGE
130 MB

Data folder size
128 MB

-------------


docker run --name redis-ctn -p 6379:6379 -v $(pwd)/data:/data -d redis redis-server --appendonly yes
go run index.go

curl http://localhost:8080/pnbas/PNNO004-1000000005 --include --request "POST";
curl http://localhost:8080/pnbas/PNNO002-1000000002
curl http://localhost:8080/pnbas

curl http://localhost:30080/pnbas/PNNO004-1000000005 --include --request "POST";
curl http://localhost:30080/pnbas/PNNO002-1000000002
curl http://localhost:30080/pnbas

docker build --tag pnbas .

docker run --name pnbas -p 8080:8080 pnbas

docker run --name pnbas \
--env REDIS_PNBAS_SERVICE_HOST=10.97.43.88 \
--env REDIS_PNBAS_SERVICE_PORT=6379 \
-p 8080:8080 pnbas


kubectl apply -f deployment.yml
kubectl apply -f deployment2.yml

kubectl delete -f deployment.yml
kubectl delete -f deployment2.yml